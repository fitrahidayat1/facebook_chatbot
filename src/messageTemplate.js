exports.messages = {
    askName: `Hello! What's your first name?`,
    askBirthdate: 'when is your birthday? Please answer in YYYY-MM-DD format.',
    askDayLeft: 'Do you want to know how many days till your next birthday?',
    showDayLeft: '5 Days left.',
    thankYou: 'Thank you, see you later!'
}