const {actionAskBirthdate} = require('./action/actionAskBirthdate')
const { actionAskDayLeft } = require('./action/actionAskDayLeft')
const { actionShowDayLeft } = require('./action/actionShowDayLeft')

// mapping of each state to the next state
exports.nextStates = {
    askName: {stateName: 'askBirthdate', stateAction: actionAskBirthdate},
    askBirthdate: {stateName: 'askDayLeft', stateAction: actionAskDayLeft},
    askDayLeft: {stateName: 'showDayLeft', stateAction: actionShowDayLeft}
}