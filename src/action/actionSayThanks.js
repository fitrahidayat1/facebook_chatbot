const { messages } = require('../messageTemplate')

exports.actionSayThanks = async (context) => {

    await context.sendText(messages.thankYou);
}