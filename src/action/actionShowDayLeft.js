const { actionCalculateDayLeft } = require('./actionCalculateDayLeft')
const { actionSayThanks } = require('./actionSayThanks')

exports.actionShowDayLeft = async (context) => {

    const positiveValidResponse = ['yes', 'yeah', 'yup']
    const negativeValidResponse = ['no', 'nah']
    const textReceived = context.event.text
    

    if (positiveValidResponse.includes(textReceived)) {

        await actionCalculateDayLeft(context)

    } else if (negativeValidResponse.includes(textReceived)) {

        await actionSayThanks(context)
    }
}