const moment = require('moment')
const { messages } = require('../messageTemplate')

exports.actionAskBirthdate = async (context) => {

    const firstName = context.event.text
    await context.sendText(`Hello ${firstName}, ` + messages.askBirthdate);
}