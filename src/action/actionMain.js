const { nextStates } = require('../stateManagement')
const { messages } = require('../messageTemplate')


exports.Main = async (context) => {

    if (!context.state.currentState) {
        context.setState({
            currentState: 'askName'
        })
        await context.sendText(messages.askName);

    } else {
        let nextState = nextStates[context.state.currentState]

        if (nextState) {

            let nextStateName = nextState.stateName
            let nextStateAction = nextState.stateAction
            
            context.setState({
                currentState: nextStateName
            })

            await nextStateAction(context)            

        } else {
            await context.sendText(messages.thankYou);
            context.setState({
                currentState: ''
            })
        }
    }
    console.log('currentState: ' + context.state.currentState);

}