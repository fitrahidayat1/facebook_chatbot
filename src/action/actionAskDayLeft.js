const moment = require('moment')
const { messages } = require('../messageTemplate')

exports.actionAskDayLeft = async (context) => {

    const now = moment()
    let birthDate = context.event.text

    birthDate = moment(birthDate, 'YYYY-MM-DD', true);

    if (birthDate.isValid()) {
        
        context.setState({
            userProfile: {
                birthDate: context.event.text
            },
            currentState: 'askDayLeft'
        })
        
        // await context.sendText(messages.askDayLeft);
        await context.sendText(`Your birthday is at ${birthDate.format('LL')}. Do you want to know how many days till your next birthday?`, {
            quickReplies: [
                {
                contentType: 'text',
                title: '✅ Yes',
                payload: 'CALCULATE_DAY_LEFT_YES',
                },
                {
                contentType: 'text',
                title: '⛔ No',
                payload: 'CALCULATE_DAY_LEFT_NO',
                },
            ],
        });
    } else {

        context.setState({
            currentState: 'askBirthdate'
        })
        await context.sendText(`Sorry, your birthday is not valid`);
        await context.sendText(`Please answer in YYYY-MM-DD format.`);
    }
}