const moment = require('moment')
const { messages } = require('../messageTemplate')

exports.actionCalculateDayLeft = async (context) => {

    var bday = moment(context.state.userProfile.birthDate).format('YYYY-MM-DD')
    var now = moment().format('YYYY-MM-DD')
 
    // calculate how many days till next birthday
    bdayNow = bday.split('-')
    bdayNow = `${now.split('-')[0]}-${bdayNow[1]}-${bdayNow[2]}`
    bdayNow = moment(bdayNow).format('YYYY-MM-DD')

    var dayleft

    if(bdayNow > now){
        dayleft = moment(bdayNow).diff(moment(now), 'days')
    } else if(bdayNow < now) {
        bdayNow = moment(bdayNow).add(1, 'year').format('YYYY-MM-DD')
        bdayNow = moment(bdayNow).format('YYYY-MM-DD')
        dayleft = moment(bdayNow).diff(moment(now), 'days')
    } else {
        dayleft = 0
    }

    // save to mongo
    // mongc.connect(url, (error, client)=>{
    //     var koleksi = client.db('bottender').collection('chats')
    //     koleksi.insertOne({
    //     'id': session.id, '_state': session._state, 'lastActivity': session.lastActivity,
    //     'platform': session.platform, 'user': session.user
    //     })
    //     client.close()
    // })

    await context.sendText(`It's ${dayleft} day(s) left till your next birthday at ${moment(bday).format('LL')}`);
    // await context.sendText(messages.showDayLeft);
}