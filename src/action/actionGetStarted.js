const {messages} = require('../messageTemplate')

exports.getStarted = async (context) => {
        
        context.setState({
                currentState: 'askName'
        })
        await context.sendSenderAction('typing_on');
        await context.sendText('Welcome to Bot Chat.');
        await context.sendText(messages.askName);

        console.log('currentState: ' + context.state.currentState);
}


