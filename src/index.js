const { router, text, payload } = require('bottender/router');
const { Main } = require('./action/actionMain')
const { getStarted } = require('./action/actionGetStarted')
const { actionCalculateDayLeft } = require('./action/actionCalculateDayLeft')
const { actionSayThanks } = require('./action/actionSayThanks')

module.exports = async function App() {
  return router([
    payload('GET_STARTED', getStarted),
    payload('CALCULATE_DAY_LEFT_YES', actionCalculateDayLeft),
    payload('CALCULATE_DAY_LEFT_NO', actionSayThanks),
    text('*', Main)
  ]);
}
